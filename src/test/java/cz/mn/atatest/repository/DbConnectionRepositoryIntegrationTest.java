package cz.mn.atatest.repository;

import cz.mn.atatest.repository.entity.DbConnectionEntity;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DbConnectionRepositoryIntegrationTest {

    @Autowired
    private DbConnectionRepository dbConnectionRepository;

    @Test
    public void givenEmptyDB_WhenFindById_ThenReturnEmptyOptional() {
        //given

        //when
        Optional<DbConnectionEntity> found = dbConnectionRepository.findById(-1L);

        //then
        assertThat(found.isPresent()).isEqualTo(false);
    }

    @Test
    public void givenSavedDbConnection_WhenFindById_ThenReturnDbConnection() {
        //given
        DbConnectionEntity dbConnectionEntity = new DbConnectionEntity();
        dbConnectionEntity.setDatabaseName("test");
        DbConnectionEntity saved = dbConnectionRepository.save(dbConnectionEntity);

        //when
        DbConnectionEntity found = dbConnectionRepository.findById(saved.getId()).get();

        //then
        assertThat(found.getDatabaseName())
                .isEqualTo(dbConnectionEntity.getDatabaseName());
    }

    @After
    public void cleanUp() {
        dbConnectionRepository.deleteAll();
    }

}