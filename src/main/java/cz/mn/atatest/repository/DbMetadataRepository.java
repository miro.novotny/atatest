package cz.mn.atatest.repository;

import cz.mn.atatest.model.dto.DbMetadata;
import cz.mn.atatest.model.exception.AtatestException;
import cz.mn.atatest.repository.entity.DbConnectionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DbMetadataRepository {

    private static final Logger log = LoggerFactory.getLogger(DbMetadataRepository.class);

    private static final String HQL_PREVIEW_TABLE = "SELECT * FROM %s.%s LIMIT ?";
    private static final String HQL_LIST_SCHEMAS = "SELECT * FROM information_schema.schemata";
    private static final String HQL_LIST_TABLES = "SELECT * FROM information_schema.tables WHERE table_schema = ?";
    private static final String HQL_LIST_COLUMNS = "SELECT * FROM information_schema.columns WHERE table_schema = ? AND table_name = ?";

    @Value("${app.db.connection.url.pattern}")
    private String dbConnectionUrlPattern;

    @FunctionalInterface
    private interface PrepareStatementFunction {
        PreparedStatement apply(Connection connection) throws SQLException;
    }

    public List<DbMetadata> listSchemas(DbConnectionEntity dbConnection) {
        return executeQuery(dbConnection, c -> c.prepareStatement(
                HQL_LIST_SCHEMAS));
    }

    public List<DbMetadata> listTables(DbConnectionEntity dbConnection, String schema) {
        return executeQuery(dbConnection, c -> {
            PreparedStatement statement = c.prepareStatement(HQL_LIST_TABLES);
            statement.setString(1, schema);
            return statement;
        });
    }

    public List<DbMetadata> listColumns(DbConnectionEntity dbConnection, String schema, String table) {
        return executeQuery(dbConnection, c -> {
            PreparedStatement statement = c.prepareStatement(HQL_LIST_COLUMNS);
            statement.setString(1, schema);
            statement.setString(2, table);
            return statement;
        });
    }

    public List<DbMetadata> previewData(DbConnectionEntity dataSource, String schema, String table, int size) {
        return executeQuery(dataSource, c -> {
            String sql = String.format(HQL_PREVIEW_TABLE, schema, table);
            PreparedStatement statement = c.prepareStatement(sql);
            statement.setInt(1, size);
            return statement;
        });
    }

    private List<DbMetadata> executeQuery(DbConnectionEntity dbDetail, PrepareStatementFunction prepareStatementFunction) {
        try (
            Connection dbConnection = getConnection(dbDetail);
            PreparedStatement statement = prepareStatementFunction.apply(dbConnection);
            ResultSet resultSet = statement.executeQuery()
        ) {
            return transform(resultSet);
        } catch (SQLException e) {
            log.error("DB querying error occurred.", e);
            throw new AtatestException("DB querying error occurred.", e);
        }
    }

    private List<DbMetadata> transform(ResultSet resultSet) throws SQLException {

        List<DbMetadata> toReturn = new ArrayList<>();
        ResultSetMetaData metaData = resultSet.getMetaData();

        while (resultSet.next()) {
            DbMetadata dbMetadata = new DbMetadata();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                dbMetadata.getData().put(metaData.getColumnName(i), resultSet.getString(i));
            }
            toReturn.add(dbMetadata);
        }
        return toReturn;
    }

    private String createConnectionUrl(DbConnectionEntity dbConnection) {
        //jdbc:mysql://localhost:3306/dbname
        //jdbc:oracle:thin:@%s:%d:%s

        String url = String.format(dbConnectionUrlPattern,
                dbConnection.getHostName(),
                dbConnection.getPort(),
                dbConnection.getDatabaseName());
        log.debug("Connection url generated: {}", url);
        return url;
    }

    private Connection getConnection(DbConnectionEntity dbConnection) {
        try {
            return DriverManager.getConnection(createConnectionUrl(dbConnection), dbConnection.getUserName(), dbConnection.getPassword());
        } catch (SQLException e) {
            log.error("DB connection error occurred.", e);
            throw new AtatestException("DB connection error occurred.", e);
        }
    }
}
