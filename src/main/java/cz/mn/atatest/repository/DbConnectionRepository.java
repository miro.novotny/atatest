package cz.mn.atatest.repository;

import cz.mn.atatest.repository.entity.DbConnectionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DbConnectionRepository extends CrudRepository<DbConnectionEntity, Long> {
}
