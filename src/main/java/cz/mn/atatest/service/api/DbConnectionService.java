package cz.mn.atatest.service.api;

import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.model.dto.DbMetadata;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface DbConnectionService {

    @Nullable DbConnection create(@NotNull DbConnection dbConnection);
    @Nullable DbConnection get(@NotNull Long id);
    @NotNull DbConnection update(@NotNull Long id, @NotNull DbConnection dbConnection);
    void delete(@NotNull Long id);

    List<DbMetadata> listSchemas(Long id);
    List<DbMetadata> listTables(Long id, String schema);
    List<DbMetadata> listColumns(Long id, String schema, String table);
    List<DbMetadata> previewData(Long id, String schema, String table, int size);
}
