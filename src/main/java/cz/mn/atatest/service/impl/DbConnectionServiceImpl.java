package cz.mn.atatest.service.impl;

import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.model.dto.DbMetadata;
import cz.mn.atatest.repository.DbConnectionRepository;
import cz.mn.atatest.repository.DbMetadataRepository;
import cz.mn.atatest.service.api.DbConnectionService;
import cz.mn.atatest.service.mapping.DbConnectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.List;

@Service
@Transactional
public class DbConnectionServiceImpl implements DbConnectionService {

    @Autowired
    private DbConnectionRepository dbConnectionRepository;
    @Autowired
    private DbMetadataRepository dbMetadataRepository;
    @Autowired
    private DbConnectionMapper mapper;

    @Override
    public DbConnection create(@NotNull DbConnection dbConnection) {
        return mapper.toDTO(dbConnectionRepository.save(mapper.toEntity(dbConnection)));
    }

    @Override
    public DbConnection get(@NotNull Long id) {
        return mapper.toDTO(dbConnectionRepository.findById(id).orElse(null));
    }

    @Override
    public @NotNull DbConnection update(@NotNull Long id, @NotNull DbConnection dbConnection) {
        return mapper.toDTO(dbConnectionRepository.save(mapper.toEntity(dbConnection)));
    }

    @Override
    public void delete(@NotNull Long id) {
        dbConnectionRepository.deleteById(id);
    }

    @Override
    public List<DbMetadata> listSchemas(Long id) {
        return dbMetadataRepository.listSchemas(mapper.toEntity(get(id)));
    }

    @Override
    public List<DbMetadata> listTables(Long id, String schema) {
        return dbMetadataRepository.listTables(mapper.toEntity(get(id)), schema);
    }

    @Override
    public List<DbMetadata> listColumns(Long id, String schema, String table) {
        return dbMetadataRepository.listColumns(mapper.toEntity(get(id)), schema, table);
    }

    @Override
    public List<DbMetadata> previewData(Long id, String schema, String table, int size) {
        return dbMetadataRepository.previewData(mapper.toEntity(get(id)), schema, table, size);
    }
}
