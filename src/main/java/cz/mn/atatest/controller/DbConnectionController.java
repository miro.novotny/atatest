package cz.mn.atatest.controller;

import cz.mn.atatest.config.SwaggerConfig;
import cz.mn.atatest.model.dto.DbConnection;
import cz.mn.atatest.model.dto.DbMetadata;
import cz.mn.atatest.model.exception.ResourceNotFoundException;
import cz.mn.atatest.service.api.DbConnectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/db")
@Api(value = "/db", tags = {SwaggerConfig.TAG_DB})
@ResponseBody
public class DbConnectionController {

    @Autowired
    private DbConnectionService dbConnectionService;

    @PostMapping
    @ApiOperation("Creates new db connection record.")
    @ResponseStatus(HttpStatus.CREATED)
    public DbConnection create(@RequestBody @Valid DbConnection dbConnection) {
        return dbConnectionService.create(dbConnection);
    }

    @GetMapping("/{id}")
    @ApiOperation("Returns db connection record.")
    public DbConnection get(@PathVariable("id") Long id) {
        DbConnection dbConnection = dbConnectionService.get(id);
        if (dbConnection != null) {
            return dbConnection;
        } else {
            throw new ResourceNotFoundException();
        }
    }

    @PutMapping("/{id}")
    @ApiOperation("Updates existing db connection record.")
    public DbConnection update(@PathVariable("id") long id, @RequestBody @Valid DbConnection dbConnection) {
        return dbConnectionService.update(id, dbConnection);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Deletes existing db connection record.")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id) {
        dbConnectionService.delete(id);
    }

    @GetMapping("/{id}/schemas")
    @ApiOperation("Returns list of db schemas for desired db.")
    public List<DbMetadata> listSchemas(@PathVariable("id") Long id) {
        return dbConnectionService.listSchemas(id);
    }

    @GetMapping("/{id}/{schema}/tables")
    @ApiOperation("Returns list of db tables for desired db/schema.")
    public List<DbMetadata> listTables(@PathVariable("id") Long id,
                                             @PathVariable("schema") String schema) {
        return dbConnectionService.listTables(id, schema);
    }

    @GetMapping("/{id}/{schema}/{table}/columns")
    @ApiOperation("Returns list of db columns for desired db/schema/table.")
    public List<DbMetadata> listColumns(@PathVariable("id") Long id,
                                              @PathVariable("schema") String schema,
                                              @PathVariable("table") String table) {
        return dbConnectionService.listColumns(id, schema, table);
    }

    @GetMapping("/{id}/{schema}/{table}/preview")
    @ApiOperation("Returns a preview of data for desired db and db/schema/table.")
    public List<DbMetadata> previewData(@PathVariable("id") Long id,
                                              @PathVariable("schema") String schema,
                                              @PathVariable("table") String table,
                                              @RequestParam(name = "size", required = false, defaultValue = "20") int size) {
        return dbConnectionService.previewData(id, schema, table, size);
    }
}
