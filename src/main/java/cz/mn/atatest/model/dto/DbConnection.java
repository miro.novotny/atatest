package cz.mn.atatest.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ApiModel(description = "DTO describing a db connection to be used when connecting/querying particular db.")
@Getter
@Setter
@ToString
public class DbConnection {

    @ApiModelProperty("the id of db connection record.")
    private Long id;
    @ApiModelProperty("the name of db connection record.")
    private String name;
    @ApiModelProperty("the hostname used in connection string.")
    private String hostName;
    @ApiModelProperty("the port used in connection string.")
    private Integer port;
    @ApiModelProperty("the db name used in connection string.")
    private String databaseName;
    @ApiModelProperty("the username to be used in connection string.")
    private String userName;
    @ApiModelProperty("the password to be used in connection string.")
    private String password;
}
