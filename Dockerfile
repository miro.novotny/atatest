FROM java:8
ADD target/atatest-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java","-jar","./atatest-0.0.1-SNAPSHOT.jar", "--port=80"]